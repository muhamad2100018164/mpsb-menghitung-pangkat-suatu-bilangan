.model small
.code

putc    macro   char
        push    ax
        mov     al, char
        mov     ah, 0eh
        int     10h     
        pop     ax
endm

org 100h       
jmp start    
output1 db "Program Pangkat Bilangan. ",0dh,0ah, '$'
output2 db "Program Bekerja Hanya Dengan Angka Integer. ", 0dh,0ah, '$' 
output3 db "Hanya Bekerja Optimal Dengan Angka Yang Tidak Terlalu Besar. ", 0dh, 0ah, '$'
nilai1 db 0dh,0ah, "Masukkan Bilangan Yang Ingin Dipangkatkan : ",'$'
nilai2 db 0dh,0ah, "Ingin Dipangkatkan Berapa? : ",'$'
hasil db 0dh,0ah, "Hasilnya Adalah = ",'$'
akhir db 0dh,0ah, "Terima Kasihhh",'$'  

num1 dw ?
num2 dw ?
ten  dw 10

start:
    mov ah,09h
    mov dx,offset output1    
    int 21h
    
    mov ah,09h
    mov dx,offset output2    
    int 21h
    
    mov ah,09h
    mov dx,offset output3    
    int 21h

    lea dx, nilai1
    mov ah,09h
    int 21h
    
    call input
    
    mov num1,cx
    
    
    lea dx, nilai2
    mov ah,09h
    int 21h
    
    call input
    
    mov num2, cx
    
    lea dx, hasil
    mov ah,09h
    int 21h
    
    jmp perhitungan
    
input       proc near     
    
    push    dx
    push    ax
    push    si
        
    mov     cx, 0 
    
    mov     cs:make_minus, 0 
input_angka:

    mov     ah, 00h
    int     16h
    mov     ah, 0eh
    int     10h
    
    cmp     al, '-'
    je      set_minus
    
    cmp     al, 0dh  
    jne     lanjuuttt
    jmp     stop_input
lanjuuttt:
    
    cmp     al, 8                   
    jne     backspace_check
    mov     dx, 0                   
    mov     ax, cx                  
    div     cs: ten                  
    mov     cx, ax
    putc    ' '                     
    putc    8                       
    jmp     input_angka
backspace_check:


        
        cmp     al, '0'
        jae     ok_ae_0
        jmp     hapus_!angka
ok_ae_0:        
        cmp     al, '9'
        jbe     ok_digit
hapus_!angka:       
        putc    8       
        putc    ' '     
        putc    8               
        jmp     input_angka        
ok_digit:


        push    ax
        mov     ax, cx
        mul     cs: ten                  
        mov     cx, ax
        pop     ax

        
        cmp     dx, 0
        jne     too_big

        
        sub     al, 30h

        
        mov     ah, 0
        mov     dx, cx      
        add     cx, ax
        jc      too_big2    

        jmp     input_angka

set_minus:
        mov     cs:make_minus, 1
        jmp     input_angka

too_big2:
        mov     cx, dx      
        mov     dx, 0       
too_big:
        mov     ax, cx
        div     cs: ten  
        mov     cx, ax
        putc    8       
        putc    ' '     
        putc    8               
        jmp     input_angka 
        
        
stop_input:
        
        cmp     cs:make_minus, 0
        je      not_minus
        neg     cx
not_minus:

        pop     si
        pop     ax
        pop     dx
        ret
make_minus      db      ?       
input        endp

perhitungan:
    
    mov ax,num1
    mov cx,num2
    dec cx
pangkat:
    
    mul num1    
    loop pangkat
    
    call print
    
    jmp akhirnya
    
print       proc    near
        push    dx
        push    ax

        cmp     ax, 0
        jnz     not_zero

        putc    '0'
        jmp     printed

not_zero:
        cmp     ax, 0
        jns     positive
        neg     ax

        putc    '-'

positive:
        call    print1
printed:
        pop     ax
        pop     dx
        ret
print       endp



print1   proc    near
        push    ax
        push    bx
        push    cx
        push    dx

        mov     cx, 1

        mov     bx, 10000       

        cmp     ax, 0
        jz      print_zero

begin_print:

        cmp     bx,0
        jz      end_print

        cmp     cx, 0
        je      calc
        
        cmp     ax, bx
        jb      skip
calc:
        mov     cx, 0   

        mov     dx, 0
        div     bx      
        
        add     al, 30h    
        putc    al


        mov     ax, dx  

skip:
        
        push    ax
        mov     dx, 0
        mov     ax, bx
        div     cs: ten  
        mov     bx, ax
        pop     ax

        jmp     begin_print
        
print_zero:
        putc    '0'
        
end_print:

        pop     dx
        pop     cx
        pop     bx
        pop     ax
        ret
print1   endp

akhirnya:
    mov ah,09h
    mov dx,offset akhir
    int 21h
    
    int 20h
    end start

